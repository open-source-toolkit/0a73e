# GCC-ARM开发环境软件包

## 简介

本仓库提供了一个在Windows平台上搭建基于“gcc + Cmake + gcc-arm-none-eabi”开源嵌入式开发工具链的资源文件。通过使用本软件包，您可以轻松地在Windows环境下配置和使用GCC-ARM开发环境，适用于嵌入式系统的开发和调试。

## 主要内容

- **gcc**: GNU Compiler Collection，用于编译C/C++代码。
- **Cmake**: 跨平台的构建工具，用于生成构建文件。
- **gcc-arm-none-eabi**: 专门用于ARM架构的嵌入式系统开发的编译器。

## 使用说明

1. **下载资源文件**: 从本仓库下载提供的资源文件。
2. **解压文件**: 将下载的压缩包解压到您希望安装的目录。
3. **配置环境变量**: 将解压后的目录路径添加到系统的环境变量中，以便在命令行中可以直接调用相关工具。
4. **验证安装**: 打开命令行工具，输入以下命令验证安装是否成功：
   ```bash
   gcc --version
   cmake --version
   arm-none-eabi-gcc --version
   ```
   如果显示版本信息，则表示安装成功。

## 注意事项

- 请确保您的系统已安装必要的依赖项，如Visual Studio Build Tools等。
- 如果在配置过程中遇到问题，请参考相关工具的官方文档或在本仓库的Issues中提出问题。

## 贡献

欢迎大家提出问题、建议或贡献代码。如果您有任何改进的想法，请提交Pull Request或Issue。

## 许可证

本仓库中的资源文件遵循相应的开源许可证。具体信息请参考每个工具的官方文档。

---

希望本资源文件能帮助您顺利搭建GCC-ARM开发环境，祝您开发顺利！